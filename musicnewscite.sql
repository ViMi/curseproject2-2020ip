-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Июн 01 2020 г., 11:08
-- Версия сервера: 8.0.15
-- Версия PHP: 7.0.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `musicnewscite`
--

-- --------------------------------------------------------

--
-- Структура таблицы `article`
--

CREATE TABLE `article` (
  `idArticle` int(11) NOT NULL,
  `idUser` int(11) DEFAULT NULL,
  `idType` int(11) DEFAULT NULL,
  `idDirection` int(11) NOT NULL,
  `articleName` varchar(124) DEFAULT NULL,
  `articlePath` varchar(255) DEFAULT NULL,
  `articleHeaderPath` varchar(255) DEFAULT NULL,
  `publicDate` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `article`
--

INSERT INTO `article` (`idArticle`, `idUser`, `idType`, `idDirection`, `articleName`, `articlePath`, `articleHeaderPath`, `publicDate`) VALUES
(1, 1, 1, 1, 'Hello newbies', 'article_1.txt', '1.jpg', '2020-04-30'),
(2, 1, 1, 1, 'Hello newbies', 'article_2.txt', '2.jpg', '2020-04-30'),
(3, 1, 1, 1, 'Hello newbies', 'article_3.txt', '3.jpg', '2020-04-30'),
(4, 1, 1, 1, 'Hello newbies', 'article_4.txt', '4.jpg', '2020-04-30'),
(5, 1, 1, 1, 'Hello newbies', 'article_2.txt', '5.jpg', '2020-04-30'),
(6, 1, 1, 3, NULL, '5.jpg', NULL, NULL),
(7, 1, 1, 3, NULL, '6.jpg', NULL, NULL),
(8, 1, 1, 3, NULL, '7.jpg', NULL, NULL),
(9, 1, 1, 3, NULL, '8.jpg', NULL, NULL),
(10, 1, 1, 3, NULL, '9.jpg', NULL, NULL),
(11, 1, 1, 3, NULL, '10.jpg', NULL, NULL),
(12, 1, 1, 3, NULL, '11.jpg', NULL, NULL),
(13, 1, 1, 3, NULL, '12.jpg', NULL, NULL),
(14, 1, 1, 3, NULL, '13.jpg', NULL, NULL),
(15, 1, 1, 3, NULL, '6.jpg', NULL, NULL),
(16, 1, 1, 3, NULL, '7.jpg', NULL, NULL),
(17, 1, 1, 3, NULL, '8.jpg', NULL, NULL),
(19, 1, 1, 2, 'Concert New Orlean', '1.mp4', NULL, NULL),
(20, 1, 1, 2, 'Concert New Orlean', '2.avi', NULL, NULL),
(21, 1, 1, 2, 'Concert New Orlean', '3.mp4', NULL, NULL),
(31, 10, 1, 1, 'TEST', 'TEST.txt', '11f009013a0b0acf9bd74537c3a823a2.jpg', '2020-06-01');

-- --------------------------------------------------------

--
-- Структура таблицы `comment`
--

CREATE TABLE `comment` (
  `idComment` int(11) NOT NULL,
  `idArticle` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `commentText` char(255) DEFAULT NULL,
  `publicDate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `comment`
--

INSERT INTO `comment` (`idComment`, `idArticle`, `idUser`, `commentText`, `publicDate`) VALUES
(1, 19, 9, 'Test Comment Alehandro :)', '2020-05-24'),
(2, 19, 1, 'Хто тут ?', '2020-05-24'),
(3, 19, 1, 'Хто тут ?', '2020-05-24'),
(4, 1, 1, 'Hi ! My Name is Vasia', '2020-05-28'),
(5, 27, 10, 'New cool article', '2020-05-30'),
(6, 29, 1, 'Hi It is my first aricle :)', '2020-05-31'),
(7, 31, 10, 'Hi it is my first article', '2020-06-01');

-- --------------------------------------------------------

--
-- Структура таблицы `direction`
--

CREATE TABLE `direction` (
  `idDirection` int(11) NOT NULL,
  `DirName` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `direction`
--

INSERT INTO `direction` (`idDirection`, `DirName`) VALUES
(1, 'article'),
(2, 'video'),
(3, 'gallery');

-- --------------------------------------------------------

--
-- Структура таблицы `message`
--

CREATE TABLE `message` (
  `idSender` int(11) DEFAULT NULL,
  `idGetter` int(11) DEFAULT NULL,
  `sendDate` date DEFAULT NULL,
  `message` varchar(256) DEFAULT NULL,
  `them` varchar(64) DEFAULT NULL,
  `idMessage` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `message`
--

INSERT INTO `message` (`idSender`, `idGetter`, `sendDate`, `message`, `them`, `idMessage`) VALUES
(9, 1, '2020-05-24', 'author stat please :|', 'AuthorStatus', 1),
(10, 1, '2020-05-30', 'Hellow ? Stardestroyer3000', 'Test Message', 2),
(1, 10, '2020-05-31', 'Hi', 'Hi', 3),
(10, 1, '2020-06-01', 'Hello ', 'TEST 2', 4);

-- --------------------------------------------------------

--
-- Структура таблицы `score`
--

CREATE TABLE `score` (
  `idArticle` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `Score` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `score`
--

INSERT INTO `score` (`idArticle`, `idUser`, `Score`) VALUES
(1, 1, 6),
(19, 8, 5),
(1, 8, 10),
(26, 8, 10),
(19, 9, 8),
(21, 1, 9),
(27, 10, 10),
(29, 1, 4),
(21, 10, 5),
(31, 10, 4);

-- --------------------------------------------------------

--
-- Структура таблицы `type`
--

CREATE TABLE `type` (
  `idType` int(11) NOT NULL,
  `typeName` char(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `type`
--

INSERT INTO `type` (`idType`, `typeName`) VALUES
(1, 'Metal'),
(2, 'Culture'),
(3, 'PopMusic'),
(4, 'other');

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `idUser` int(11) NOT NULL,
  `idUType` int(11) NOT NULL,
  `userLogin` char(32) NOT NULL,
  `userPassword` char(32) NOT NULL,
  `RegDate` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`idUser`, `idUType`, `userLogin`, `userPassword`, `RegDate`) VALUES
(1, 1, 'STARDESTROYER', 'VitaLaVita', '2020-04-29'),
(2, 2, 'Victor', '25021999', '2020-04-30'),
(6, 2, 'AlehandroGonzales', '25021999', '2020-04-30'),
(9, 2, 'Alehandro', '123', '2020-05-24'),
(10, 1, 'Tomas', '250299', '2020-05-30');

-- --------------------------------------------------------

--
-- Структура таблицы `usertype`
--

CREATE TABLE `usertype` (
  `idUType` int(11) NOT NULL,
  `typeName` char(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `usertype`
--

INSERT INTO `usertype` (`idUType`, `typeName`) VALUES
(1, 'admin'),
(2, 'user'),
(3, 'author');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`idArticle`),
  ADD KEY `idUser` (`idUser`),
  ADD KEY `idType` (`idType`),
  ADD KEY `article_ibfk_3` (`idDirection`);

--
-- Индексы таблицы `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`idComment`) USING BTREE,
  ADD KEY `idUser` (`idUser`);

--
-- Индексы таблицы `direction`
--
ALTER TABLE `direction`
  ADD PRIMARY KEY (`idDirection`);

--
-- Индексы таблицы `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`idMessage`);

--
-- Индексы таблицы `type`
--
ALTER TABLE `type`
  ADD PRIMARY KEY (`idType`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`idUser`),
  ADD UNIQUE KEY `userLogin` (`userLogin`),
  ADD KEY `idUType` (`idUType`);

--
-- Индексы таблицы `usertype`
--
ALTER TABLE `usertype`
  ADD PRIMARY KEY (`idUType`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `article`
--
ALTER TABLE `article`
  MODIFY `idArticle` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT для таблицы `message`
--
ALTER TABLE `message`
  MODIFY `idMessage` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `type`
--
ALTER TABLE `type`
  MODIFY `idType` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `idUser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `usertype`
--
ALTER TABLE `usertype`
  MODIFY `idUType` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `article`
--
ALTER TABLE `article`
  ADD CONSTRAINT `article_ibfk_1` FOREIGN KEY (`idUser`) REFERENCES `user` (`idUser`) ON UPDATE CASCADE,
  ADD CONSTRAINT `article_ibfk_2` FOREIGN KEY (`idType`) REFERENCES `type` (`idType`) ON UPDATE CASCADE,
  ADD CONSTRAINT `article_ibfk_3` FOREIGN KEY (`idDirection`) REFERENCES `direction` (`idDirection`) ON DELETE RESTRICT ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `comment`
--
ALTER TABLE `comment`
  ADD CONSTRAINT `comment_ibfk_1` FOREIGN KEY (`idUser`) REFERENCES `user` (`idUser`) ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`idUType`) REFERENCES `usertype` (`idUType`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
