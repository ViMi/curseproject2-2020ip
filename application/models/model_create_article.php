<?php

namespace Model;

include "model_content_page.php";

/**
 * Class Model_Create_Article For Author Right Level
 */
class Model_Create_Article extends \Model\Model_Content_Page {


//----------------------------------------------------------------INFORMATION FUNCTIONS

    //----------------------------[Article Info]

    /**
     * Return pdo obj which contains article by author or null
     * @param $authorName
     * @return |null
     */
    public function getArticleInfoByAuthor($authorName) {

        if(isset($authorName)) {

            $query = new \Core\Database_Query('article');
            $query->select("
               article.idArticle,
            userLogin,
            type.typeName AS articleType,
            DirName,
            articleName,
            articlePath,
            articleHeaderPath,
            article.publicDate,
            userType.typeName as userType
            ")->join('user', 'user.idUser=article.idUser')
                ->join("direction", 'direction.idDirection=article.idDirection')
                ->join('userType', "userType.idUType=user.idUType")
                ->join('type', 'type.idType=article.idType')
                ->where(["userLogin" => $authorName]);

            return $this->_queryExecute($query);

        } else {
            return null;
        }
    }


    /**
     * Return pdo object which contains article info by name
     * @param $articleName
     * @return |null
     */
    public function getArticleInfoByName($articleName) {

        if(isset($articleName)) {

            $query = new \Core\Database_Query('article');
            $query->select("
               article.idArticle,
            userLogin,
            type.typeName AS articleType,
            DirName,
            articleName,
            articlePath,
            articleHeaderPath,
            article.publicDate,
            userType.typeName as userType
            ")->join('user', 'user.idUser=article.idUser')
                ->join("direction", 'direction.idDirection=article.idDirection')
                ->join('userType', "userType.idUType=user.idUType")
                ->join('type', 'type.idType=article.idType')
                ->where(["articleName" => $articleName]);


            return $this->_queryExecute($query);

        } else {
            return null;
        }

    }


    /**
     * Return pdo obj which contains article info by id or null
     * @param $articleId
     * @return |null
     */
    public function getArticleInfoById($articleId) {
        if(isset($articleId)) {

            $query = new \Core\Database_Query('article');
            $query->select("
               article.idArticle,
            userLogin,
            type.typeName AS articleType,
            DirName,
            articleName,
            articlePath,
            articleHeaderPath,
            article.publicDate,
            userType.typeName as userType
            ")->join('user', 'user.idUser=article.idUser')
                ->join("direction", 'direction.idDirection=article.idDirection')
                ->join('userType', "userType.idUType=user.idUType")
                ->join('type', 'type.idType=article.idType')
                ->where(["idArticle" => $articleId]);

            return $this->_queryExecute($query);

        } else {
            return null;
        }



    }

    //------------------------------[TypeInfo]

    /**
     * Return pdo obj which contains all types
     * @return |null
     */
    public function getTypes() {
        $query = new \Core\Database_Query('type');
        $query->select("*");
        return $this->_queryExecute($query);

    }


    //------------------------------[Directions Info]

    /**
     * Return pdo obj which contains all directions
     * @return |null
     */
    public function getDirections() {

        $query = new \Core\Database_Query('direction');
        $query->select("*");

        return $this->_queryExecute($query);

    }


//----------------------------------------------------------------ADD FUNCTIONS

    //------------------------------[Add Article]

    /**
     * Atomar oreration to add new record in to DB and save files to Server Memory (if false restore start state of sys)
     * @param $articleText
     * @param $articleName
     * @param $authorId
     * @param $articleType
     * @return bool
     *
     */
    public function addNewArticle($articleText, $articleName, $authorId, $articleType) {

        if(isset($articleText) && isset($articleName) && isset($authorId) && isset($articleType)) {


            if ($this->_addArticleTextFile($articleText, $articleName.'.txt')) {

                if ($headerName = $this->_addArticleHeader()) {

                    if ($this->_addNewArticle($authorId, $articleName, $articleName, $headerName, $articleType)) {

                        return true;
                    } else {
                        $this->_deleteArticleFile($articleName.'.txt');
                        $this->_deleteHeaderFile($headerName);
                        return false;
                    }
                } else {

                    $this->_deleteArticleFile($articleName.'.txt');
                    return false;
                }
            } else {
                return false;
            }
        } else {
           return false;
        }
    }


    //--------[Service functions]

                //------------[Add]

    protected function _addNewArticle($authorId, $articleFile, $articleName, $articleHeaderName, $articleType, $direction='article') {

        if($idType = $this->_getIdTypeByName($articleType)) {
            if($idDirection = $this->_getIdDirectionByName($direction)) {

                $publicDate = date('y.m.d');
                $articlePath = $articleName . ".txt";
                $articleHeaderPath = $articleHeaderName;

                $query = new \Core\Database_Query("article");
                $query->insert([
                    "idUser"=>$authorId,
                    "articleName"=>$articleName,
                    "articlePath"=>$articlePath,
                    "articleHeaderPath"=>$articleHeaderPath,
                    "publicDate"=>$publicDate,
                    "idDirection"=>$idDirection,
                    "idType"=>$idType
                ]);
                    try {
                          $this->_queryExecute($query);
                        return true;
                    } catch (\PDOException $e) {
                        return false;
                    }
            }
            else {
                return false;
            }
        }
        else
        {
            return false;
        }
    }


    protected function _addArticleTextFile($articleText, $articleName) {

        if(sizeof($articleText) <= 1024*1024*1024) {

            $str = htmlentities(strip_tags($articleText));

            $filePath = 'resources\articleText\\' . $articleName;
            $file = null;
            if ($file = fopen($filePath, 'w')) {
                fwrite($file, $str);
                fclose($file);
            } else {
                return false;
            }

            return true;
        }

        return false;
    }


    protected function _addArticleHeader() {

        if($_FILES['headerImg']['size'] != 0 && $_FILES['headerImg']['size'] <= 1048576){
            $photo = 'resources\pictures\articles\\'.$_FILES['photo']['name'];

            @mkdir('articleHeader',0777);

            copy($_FILES['headerImg']['tmp_name'], "resources\pictures\articles\\".basename($_FILES['headerImg']['name']));

            return basename($_FILES['headerImg']['name']);
        }
        else {
            return false;
        }

    }


    protected function _deleteHeaderFile($fileName) {
        try {
             unlink('resources\pictures\articles\\' . $fileName);
        } catch (\Exception $e) {
            return false;
        }
        return true;
    }


    protected function _deleteArticleFile($fileName) {
        try {
            return unlink('resources\articleText\\' . $fileName);
        } catch (\Exception $e) {
            return false;
        }
    }


    protected function _getIdTypeByName($typeName) {

        $query = new \Core\Database_Query('type');
        $query->select("idType")
            ->where(["typeName"=>$typeName]);

        try {
            $res = $this->_queryExecute($query);
            $res = $res->fetch();
            return $res['idType'];
        } catch (\PDOException $e) {
            return false;
        }

    }


    protected function _getIdDirectionByName($direction) {
        $query = new \Core\Database_Query('direction');
        $query->select("idDirection")
            ->where(["DirName"=>$direction]);

        try {
            $res = $this->_queryExecute($query);
            $res = $res->fetch();
            return $res['idDirection'];
        } catch(\PDOException $e) {
         return false;
        }

    }


    public function jsonInit() {

    $dirPdo = $this->getDirections();

    while($dirType = $dirPdo->fetch()) {

            $result = null;
            $searchResultInfo = null;

            $data = array(
                'direction' => $dirType['idDirection'],
            );

            $this->search($data, $result, $searchResultInfo);

            $resStr = "[ \n";
            while($resultSearch = $searchResultInfo->fetch()) {
                $resStr = $resStr.json_encode($resultSearch).",\n";
            }
            $resStr = substr($resStr, 0 , strlen($resStr)-2);
            $resStr = $resStr."\n]\n";
            $this->_jsonFrom($dirType['DirName'], $resStr, 'w');
    }

}


    protected function _jsonFrom($nameType, $res, $reg) {
        $filePath = 'json\\'.$nameType.'.json';
        if($file = fopen($filePath, $reg)) {
            fwrite($file, $res);
            fclose($file);
            return true;
        } else {
            return false;
        }
    }


                //------------[Delete]

    protected function _removeArticle($articleData) {

        try {
        $articleData = $articleData->fetch();

        if(isset($articleData)) {


            $article = "resources\articleText\\".$articleData['articlePath'];

            if(file_exists($article) && $articleData['articlePath'])
            {
                $textContent = $this->readArticleTextFile($article);
                $stage1=($this->_deleteArticleFile($articleData['articlePath']) && $textContent);
            } else {
                $stage1 = true;
            }

            if ($stage1) {


                $header = "resources\pictures\articles\\".$articleData['articleHeaderPath'];


                if(file_exists($header) && $articleData['articleHeaderPath']) {
                    $image = $this->_readHeader($header, $articleData['articleHeaderPath']);
                    $stage2 = ($this->_deleteHeaderFile($articleData['articleHeaderPath']) && $image);
                } else {
                    $stage2 = true;
                }
                if ($stage2) {

                    return true;

                } else {
                    if(isset($image)) {
                        $this->_restoreHeader($image, $header, $articleData['articleHeaderPath']);
                    }
                    if(isset($textContent)) {
                        $this->_addArticleTextFile($textContent, $articleData['articlePath']);
                    }
                    return false;

                }

            } else {

                if(isset($textContent)) {
                    $this->_addArticleTextFile($textContent, $articleData['articlePath']);
                }

                return false;
            }
        } else {
            return false;
        } } catch (\PDOException $e){
            echo $e->getMessage();
            return false;
        }
    }

    //------------------------------[Read Header]

    protected function _readHeader($path, $headerName) {

        $extension = explode('.', $headerName)[1];

        if(file_exists($path)) {
            switch($extension) {
                case 'jpg':
                case 'JPEG':
                 {
                    return imagecreatefromjpeg($path);
                }

                case 'png':
                {
                    return imagecreatefrompng($path);
                }

                default:
                {
                    return false;
                }

            }
          }
        else {
            echo "<script> alert('File not find'); </script>";
            return false;
        }

    }


    protected function _restoreHeader($image, $path, $headerName) {

        if(isset($headerName) && isset($image) && isset($path)) {
            $extension = explode('.', $headerName)[1];

            switch ($extension) {

                case 'jpg':
                case 'JPEG':
                {
                    imagejpeg($image, $path . '\\' . $headerName);
                    return true;
                }

                case 'png':
                {
                    imagepng($image, $path . '\\' . $headerName);
                    return true;
                }

                default:
                {
                    return false;
                }
            }
        } else {
            return false;
        }
    }


    //--------------------------------------------------------------REMOVE FUNCTIONS


    //--------------------------------[Remove Articles]

    /**
     * Return article record by id (Author right level)
     * @param $idArticle
     * @param $idUser
     * @return bool
     */
    public function removeArticleById($idArticle, $idUser) {

        if(isset($idArticle)) {

            if ($this->_removeArticle($this->getArticleInfoById($idArticle))) {

                $query = new \Core\Database_Query('article');
                $query->delete()
                    ->where(["idArticle"=>$idArticle, "idUser"=>$idUser]);

                return $this->_queryExecute($query);

            } else {
                return false;
            }
        } else {
            return false;
        }
    }


    /**
     * Remove article record by name (Author right level)
     * @param $nameArticle
     * @param $idUser
     * @return bool
     */
    public function removeArticleByName($nameArticle, $idUser) {


        if(isset($nameArticle) && isset($idUser)) {

            if($this->_removeArticle($this->getArticleInfoByName($nameArticle))) {


                $query = new \Core\Database_Query('article');
                $query->delete()
                    ->where(["articleName"=>$nameArticle, "idUser"=>$idUser]);

            return $this->_queryExecute($query);

        } else {
            return false;
        } } else {
            return false;
        }
    }


}