<?php

namespace Model;

include "model_content_page.php";
/**
 * Class Model_Registration for  User Right Level
 */
class Model_Registration extends \Model\Model_Content_Page {
//---------------------------------------------------------------ADD FUNCTIONS

    //------------------------------------[User Add]

    /**
     * Add New User Into sys DB
     * @param $login
     * @param $pass
     * @param $date
     * @param $userRole
     * @return bool
     */
    public function addNewUser($login, $pass, $date, $userRole) {


        if(isset($login) && isset($pass) && isset($date) && isset($userRole)) {


            $query = new \Core\Database_Query('user');
            $query->insert([
                "idUType"=>$userRole,
                "userLogin"=>$login,
                "userPassword"=>$pass,
                "RegDate"=>$date]);
            return $this->_queryExecute($query);

        } else {
            return false;
        }
    }

//--------------------------------------------------------------REMOVE FUNCTIONS

    //-----------------------------------------[User Remove]

    /**
     * Remove user Record from sys DB
     * @param $login
     * @param $pass
     * @return bool
     */
    public function removeUser($login, $pass) {

        if(isset($login) && isset($pass)) {

            $query = new \Core\Database_Query('user');
            $query->delete()
                ->where(["userLogin"=>$login, "userPassword"=>$pass]);

           return  $this->_queryExecute($query);

        } else {
            return false;
        }
    }


//--------------------------------------------------------------UPDATE FUNCTIONS

    //----------------------------------------[User update]

    /**
     * Update user info
     * @param $login
     * @param $pass
     * @param $idUser
     * @return bool
     */
    public function updateUserInfo($login, $pass, $idUser) {

        if(isset($login) && isset($pass) && isset($idUser)) {

            $query = new \Core\Database_Query('user');
            $query->update(["userLogin"=>$login, "userPassword"=>$pass])
                ->where(["idUser"=>$idUser]);

            if($this->_queryExecute($query)) {
                return true;
            }
            return false;

        } else {
            return false;
        }
    }


}