<?php

namespace Model;

/**
 * Class Model_Content_Page to user right level
 */
class Model_Content_Page extends \Core\Model {
    //----------------------------------------OTHER

    /**
     * Return pdo obj which contains search result or null
     * @param $data
     * @param $result
     * @param $searchResultInfo
     * @return |null
     */
    public function search($data, &$result, &$searchResultInfo) {

        global $database;


        if(isset($data)) {

    $direction = $data['direction'];
    $typeOfSearch = $data['typeOfSearch'];
    $searchNameField = $data['searchNameField'];
    $articleType = $data['articleType'];

    $querySearchSubQuery = null;
    $whereSubQuery = null;


    $query = new \Core\Database_Query("article");


    if (isset($searchNameField) && strlen(trim($searchNameField)) > 0) {

        $nameSearch = htmlentities(strip_tags($searchNameField));
        $queryTypeSearch = "(idDirection=$direction) AND (articleName='$nameSearch')";

    } else {
        $queryTypeSearch = "(idDirection=$direction)";
    }


    if (isset($typeOfSearch) && isset($articleType)) {

        if ($articleType != 'all') {
            $querySearchSubQuery = "JOIN type ON type.idType = article.idType";
            $whereSubQuery = "(type.typeName='$articleType') AND";
        }


        switch ($typeOfSearch) {
            case 'authorName':
                {

                    if (isset($querySearchSubQuery)) {

                        $nameSearch = htmlentities(strip_tags($searchNameField));
                        $querySearchSubQuery = $querySearchSubQuery . " JOIN user ON user.idUser=article.idUser";
                        $queryTypeSearch = "(idDirection=$direction) AND (userLogin='$nameSearch')";
                    } else {

                        $nameSearch = htmlentities(strip_tags($searchNameField));
                        $querySearchSubQuery = "JOIN user ON user.idUser=article.idUser";
                        $queryTypeSearch = "(idDirection=$direction) AND (userLogin='$nameSearch')";
                    }

                }
                break;
        }

    }


    if (isset($whereSubQuery)) {
        $whereSubQuery = $whereSubQuery . $queryTypeSearch;
    } else {
        $whereSubQuery = $queryTypeSearch;
    }


    $queryCount = "
SELECT 
       COUNT(*) AS count 
FROM 
     article 
         $querySearchSubQuery 
WHERE 
      $whereSubQuery";

    $querySearch = "
    SELECT 
           idArticle,
           articleName,
           articlePath,
           articleHeaderPath
    FROM article $querySearchSubQuery
    WHERE $whereSubQuery";


    $res = $database->pdo->query($queryCount);

    if ($res) {
        $result = $res->fetch();

        $searchResultInfo = $database->pdo->query($querySearch);

    }
} else {
    return null;
}
        return null;
    }

//---------------------------------------INFORMATION FUNCTION

    /**
     * Return pdo obj which contains all article information
     * @param $id
     * @return bool
     */
    public function getArticleInfo($id) {
        if(isset($id)) {

            $query = new \Core\Database_Query('article');
            $query->select("articleName, typeName, userLogin, articleHeaderPath, articlePath, publicDate")
                ->join('user', 'user.idUser = article.idUser')
                ->join('type', 'type.idType = article.idType')
                ->where(['idArticle' => $id]);

            $res = $this->_queryExecute($query);
             return $res->fetch();

        } else {
            return false;
        }
    }

//---------------------------------------LOGIN USER

    /**
     * Log In user in DB sys or return false
     * @param $login
     * @param $pass
     * @return bool
     */
    public function logInUser($login, $pass) {

        if(isset($login) && isset($pass)){

            $query = new \Core\Database_Query('user');
            $query->select("userLogin, userPassword, idUser, typeName")->
                join('userType', 'userType.idUType = user.idUType')->
                where(['userLogin' => "{$login}", 'userPassword'=>"{$pass}"]);


                $res = $this->_queryExecute($query);

                while($result = $res->fetch()){
                    if($result['userLogin'] === $login){
                        return $result;
                    }
                }

        return false;
        }
        else {
            return false;
        }
    }

//----------------------------------------READ FUNCTION

    /**
     * Read txt file which contains article content and return it content
     * @param $filePath
     * @return bool|false|string
     */
    public function readArticleTextFile($filePath) {

        if(file_exists($filePath)) {
            if ($contentStr = file_get_contents($filePath)) {
                return $contentStr;
            } else {
                echo "<script> alert('File not find'); </script>";
                return false;
            }
        }

        return false;
    }


    //---------------------------------------------SERVICES FUNCTION

    protected function _getUserIdByName($userName) {

        $query = new \Core\Database_Query('article');
        $query->select('idArticle')
            ->join('user', 'user.idUser = article.idUser')
            ->where(["userLogin"=> $userName]);

        return $this->_queryExecute($query);

    }


    protected function _getIdUserByName($authorName) {

        $query = new \Core\Database_Query('user');
        $query->select("idUser")
            ->where(['userLogin' => $authorName]);

        $authorName = $this->_queryExecute($query);
        $authorName = $authorName->fetch();
        return $authorName['idUser'];

    }


    protected function _queryExecute($query) {
        global $database;
        return $database->executeQuery($query);
    }

}
