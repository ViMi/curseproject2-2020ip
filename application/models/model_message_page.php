<?php
namespace Model;

include "model_user_page.php";

/***
 * Class Model_Message_Page for message page
 * @package Model
 */
class Model_Message_Page extends \Model\Model_User_Page {


    /***
     * Get message information and add new message in DB table
     * @param $sender
     * @param $recep
     * @param $messageText
     * @param $them
     */
    public function addNewMessage($sender, $recep, $messageText, $them){
        if(isset($sender) && isset($recep) && isset($messageText) && isset($them)) {
            $query = new \Core\Database_Query('message');
            if($this->_checkUserExist($recep)) {

                $sender = $this->_getIdUserByName($sender);
                $recep = $this->_getIdUserByName($recep);
                $date = date("y.m.d");

                if($recep) {
                    $query->insert([
                        "idSender"=>$sender,
                        "idGetter"=>$recep,
                        "sendDate"=>$date,
                        "message"=>$messageText,
                        "them"=>$them]);
                    $this->_queryExecute($query);
                }
            }
        }
    }

//-------------------------------------------GET MESSAGE


    /***
     * Return pdo object which contains message info (get message (by getter id))
     * @param $getter
     * @return bool|\PDOStatement|null|array
     */
    public function getAllMessage($getter) {

        $query = new \Core\Database_Query("message");
        $query->select("*")
            ->join("user", "idUser=idSender")
            ->where(['idGetter'=>$getter]);

        $message = $this->_queryExecute($query);

        $query = new\Core\Database_Query('user');
        $query->select("userLogin")
            ->where(['idUser'=>$getter]);

        $getter = $this->_queryExecute($query)->fetch()['userLogin'];

        return ["message"=>$message, "getter"=>$getter];
    }


    /***
     * Return pdo object which contains message info (sort by date)
     * @param $date
     * @param $getter
     * @return bool|\PDOStatement|null|array
     */
    public function getAllMessageByDate($date, $getter){

        if(isset($date) && isset($getter)) {

            $query = new \Core\Database_Query('message');
            $query->select("*")
                ->join('user',"idUser=idSender")
                ->where(['sendDate'=>$date, 'idGetter'=>$getter]);

            $message = $this->_queryExecute($query);

            $query = new\Core\Database_Query('user');
            $query->select("userLogin")
                ->where(['idUser'=>$getter]);

            $getter = $this->_queryExecute($query)->fetch()['userLogin'];

            return ["message"=>$message, "getter"=>$getter];

        } else {
            return false;
        }
    }


    /***
     * Return pdo object which contains message info (send message)
     * @param $name
     * @param $getter
     * @return bool|\PDOStatement|null|array
     */
    public function getAllMessageBySenderName($name, $getter) {

        if(isset($name) && isset($getter)) {

                $id = $this->_getIdUserByName($name);

                $query = new \Core\Database_Query('message');
                $query->select("*")
                    ->join('user',"idUser=idGetter")
                    ->where(['idSender'=>$id, 'idGetter'=>$getter]);

            $message = $this->_queryExecute($query);

            $query = new\Core\Database_Query('user');
            $query->select("userLogin")
                ->where(['idUser'=>$id]);

            $sender = $this->_queryExecute($query)->fetch()['userLogin'];

            return ["message"=>$message, "sender"=>$sender];


        }
        return false;
    }

    //-------------------------------------------[Send message]

    /***
     * Return pdo object which contains message info (get message)
     * @param $sender
     * @return bool|\PDOStatement|null|array
     */
    public function getAllSendMessage($sender) {

        $query = new \Core\Database_Query("message");
        $query->select("*")
            ->join('user', "idUser=idGetter")
            ->where(['idSender'=>$sender]);

        $message = $this->_queryExecute($query);

        $query = new\Core\Database_Query('user');
        $query->select("userLogin")
            ->where(['idUser'=>$sender]);

        $sender = $this->_queryExecute($query)->fetch()['userLogin'];

        return ["message"=>$message, "sender"=>$sender];
    }


    /***
     * Return pdo object which contains message info (sort by date)
     * @param $date
     * @param $sender
     * @return bool|\PDOStatement|null|array
     */
    public function getAllSendMessageByDate($date, $sender){

        if(isset($date) && isset($sender)) {

            $query = new \Core\Database_Query('message');
            $query->select("sendDate, message, 
            them, user.userLogin AS sender")
                ->join('user', 'idUser=idGetter')
                ->where(['sendDate'=>$date, 'idSender'=>$sender]);

            $message = $this->_queryExecute($query);


            $query = new\Core\Database_Query('user');
            $query->select("userLogin")
                ->where(['idUser'=>$sender]);
            $sender = $this->_queryExecute($query)->fetch()['userLogin'];



            return ["message"=>$message, "sender"=>$sender];

        } else {
            return false;
        }
    }


    /***
     * Return pdo object which contains message info (send message)
     * @param $name
     * @param $sender
     * @return bool|\PDOStatement|null|array
     */
    public function getAllSendMessageByGetterName($name, $sender) {

        if(isset($name) && isset($sender)) {

            $id = $this->_getIdUserByName($name);
            $query = new \Core\Database_Query('message');
            $query->select("*")
                ->join('user',"idUser=idGetter")
                ->where(['idGetter'=>$id, 'idSender'=>$sender]);

            $message = $this->_queryExecute($query);

            $query = new\Core\Database_Query('user');
            $query->select("userLogin")
                ->where(['idUser'=>$sender]);

            $sender = $this->_queryExecute($query)->fetch()['userLogin'];

            return ["message"=>$message, "sender"=>$sender];


        }
        return false;
    }

//-----------------------------------------SERVICE FUNCTION

    protected function _checkUserExist($userName) {

        $query = new \Core\Database_Query('user');
        $query->select("COUNT(*) AS record")
            ->where(['userLogin' => $userName]);
        return $this->_queryExecute($query)->fetch()['record'];

    }


}