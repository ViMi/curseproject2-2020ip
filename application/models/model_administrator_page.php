<?php

namespace Model;

include "model_create_article.php";

/**
 * Model fof administrator right level
 */
class Model_Administrator_Page extends \Model\Model_Create_Article {
 //---------------------------------------------OTHER


    /**
     * Change user Role (user, author, admin)
     * @param $userName
     * @param $newRole
     * @return null
     */
    public function changeUserRole($userName, $newRole) {
        $query = new \Core\Database_Query("user");
        $query->update(["idUType"=>$newRole])->where(["userLogin"=>$userName]);
        return $this->_queryExecute($query);
    }

//-----------------------------------------INFORMATION QUERY

    //-------------[Roles Info]

    /**
     * Return pdo obj which contains all roles
     * @return |null
     */
    public function getAllRoles() {

        $query = new \Core\Database_Query('userType');
        $query->select("*");

        return $this->_queryExecute($query);

    }

    //-------------[Comments Info]

    /**
     * Return pdo obj which contains all comments from article item or null
     * @param $articleId
     * @return |null
     */
    public function getAllCommentsByArticle($articleId) {

        $query = new \Core\Database_Query('comment');
        $query->select("*")->where(["idArticle"=>$articleId]);
        return $this->_queryExecute($query);
    }


    /**
     * Return pdo obj which contains all comments by author or null
     * @param $authorName
     * @return |null
     */
    public function getAllCommentsByAuthor($authorName) {

        $query = new \Core\Database_Query('comment');
        $query->select("*")
            ->join('user', "user.idUser=comment.idUser")
            ->where(["user.UserLogin" => $authorName]);

        return $this->_queryExecute($query);

    }


    /**
     * Return pdo obj which contains all comments or null
     * @return |null
     */
    public function getAllComments() {
        $query = new \Core\Database_Query('comment');
        $query->select("*");
       return $this->_queryExecute($query);
    }

    //---------------[User Info]

    /**
     * Return pdo obj which contains all reg users or null
     * @return |null
     */
    public function getAllRegUser() {

        $query = new \Core\Database_Query("user");
        $query->select("user.idUser, user.userLogin, user.RegDate, userType.typeName")
            ->join("userType", "userType.idUType=user.idUType");

       return $this->_queryExecute($query);
    }


    /**
     * Return pdo obj which contains all reg user which have user role or null
     * @return |null
     */
    public function getAllUsers() {

        $query = new \Core\Database_Query("user");
        $query->select("user.IdUser, user.userLogin, user.RegDate, userType.typeName")
            ->join("userType", "userType.idUType=user.idUType")
            ->where(["userType.typeName"=>"user"]);
      return $this->_queryExecute($query);

    }


    /**
     * Return pdo obj which contains all reg user which have admin role or null
     * @return |null
     */
    public function getAllAdmins() {

        $query = new \Core\Database_Query("user");
        $query->select("user.IdUser, user.userLogin, user.RegDate, userType.typeName")
            ->join("userType", "userType.idUType=user.idUType")
            ->where(["userType.typeName"=>"admin"]);
        return $this->_queryExecute($query);

    }


    /**
     * Return pdo obj which contains all reg user which have author role or null
     * @return |null
     */
    public function getAllAuthors() {
        $query = new \Core\Database_Query("user");
        $query->select("user.IdUser, user.userLogin, user.RegDate, userType.typeName")
            ->join("userType", "userType.idUType=user.idUType")
            ->where(["userType.typeName"=>"author"]);
        return $this->_queryExecute($query);
    }

    //----------------[Score Info]

    /**
     * Return pdo obj which contains all scores
     * @return |null
     */
    public function getAllScores() {

        $query = new \Core\Database_Query("score");
        $query->select("*");
        return $this->_queryExecute($query);
    }

    //---------------[Article Info]

    /**
     * Return pdo obj which contains information about all exist article or null
     * @return |null
     */
    public function getAllArticleInfo() {

        $query = new \Core\Database_Query('article');
        $query->select(
            "
               article.idArticle,
            userLogin,
            type.typeName AS articleType,
            DirName,
            articleName,
            articlePath,
            articleHeaderPath,
            article.publicDate,
            userType.typeName as userType
            ")->join("user", "user.idUser=article.idUser")
            ->join("direction", "direction.idDirection=article.idDirection")
            ->join("userType", "userType.idUType=user.idUType")
            ->join('type', "type.idType=article.idType");

        return $this->_queryExecute($query);
    }

//-------------------------------------REMOVE QUERY

    //------------[Article remove]

    /**
     * Remove record all articles by author or return false
     * @param $authorName
     * @return bool
     */
    public function removeArticleAuthor($authorName) {

        if(isset($authorName)) {

            $authorName = $this->_getUserIdByName($authorName);

                while ($id = $authorName->fetch()) {
                    $this->removeArticleById($id['idArticle']);
                }
            return true;

        } else
        {
            return false;
        }

    }


    /**
     * Remove article record by name or false
     * @param $articleName
     * @param null $idUser
     * @return bool
     */
    public function removeArticleByName($articleName, $idUser=null) {


        if(isset($articleName)) {

            if ($this->_removeArticle($this->getArticleInfoByName($articleName))) {

                $query = new \Core\Database_Query('article');
                $query->delete()->where(["articleName"=>$articleName]);
                return $this->_queryExecute($query);

            } else {
                return false;
            }
        } else
        {
            return false;
        }
    }



    /**
     * Remove article record by id or false
     * @param $idArticle
     * @param null $idUser
     * @return bool
     */
    public function removeArticleById($idArticle, $idUser=null) {

        if(isset($idArticle)) {

            if ($this->_removeArticle($this->getArticleInfoById($idArticle))) {


                $query = new \Core\Database_Query('article');
                $query->delete()->where(["idArticle"=>$idArticle]);
                return $this->_queryExecute($query);

            } else {
                return false;
            }
        } else
        {
            return false;
        }
    }


    //---------[Comment remove]

    /**
     * Remove record comment by id of return false
     * @param $idComment
     */
    public function removeCommentById($idComment) {

        $query = new \Core\Database_Query('comment');
        $query->delete()->where(["idComment"=>$idComment]);
        $this->_queryExecute($query);

    }


    /**
     * Remove all comments by author or return false
     * @param $authorName
     */
    public function removeCommentByAuthor($authorName) {

        $authorName = $this->_getIdUserByName($authorName);

        $query = new \Core\Database_Query('comment');
        $query->delete()->where(["idUser"=>$authorName]);
        $this->_queryExecute($query);

    }


    /**
     * Remove all comments by article or return false
     * @param $idArticle
     */
    public function removeCommentByArticle($idArticle) {


        $query = new \Core\Database_Query('comment');
        $query->delete()->where(["idArticle"=>$idArticle]);
        $this->_queryExecute($query);

    }


    //------------[User remove]

    /**
     * Remove user record by id or return false
     * @param $idUser
     */
    public function removeUserById($idUser) {

        $query = new \Core\Database_Query('user');
        $query->delete()->where(["idUser"=>$idUser]);

        $this->_queryExecute($query);
    }


    /**
     * Remove user by name or return false
     * @param $userLogin
     */
    public function removeUserByName($userLogin) {
        $query = new \Core\Database_Query('user');
        $query->delete()->where(["userLogin"=>$userLogin]);

        $this->_queryExecute($query);

    }


}