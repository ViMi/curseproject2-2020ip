<?php

namespace Model;

include "model_content_page.php";
/**
 * Class Model_Article to user right level
 */
class Model_Article extends \Model\Model_Content_Page {

//--------------------------------------------------------ADD FUNCTIONS

    //------------------------------------[Add Article score]

    /**
     * Add new record article score
     * @param $idArticle
     * @param $idUser
     * @param $score
     */
    public function addArticleScore($idArticle, $idUser, $score) {

        $query = new \Core\Database_Query('score');
        $query->insert(["idArticle"=>$idArticle, "idUser"=>$idUser, "Score"=>$score]);

        $this->_queryExecute($query);

    }


    //------------------------------------[Add Comment Score]

    /**
     * Add new article comment
     * @param $idArticle
     * @param $loginAuthor
     * @param $commentText
     * @param $dateComment
     */
    public function addArticleComment($idArticle, $loginAuthor, $commentText, $dateComment) {
        $commentCount = $this->getArticleCommentsCount($idArticle);
        $commentCount +=1;
        $dateComment = date('y.m.d');
            $query = new \Core\Database_Query("comment");
            $query->insert(
                ["idComment"=>$commentCount,
                    "idArticle"=>$idArticle,
                    "idUser"=>$loginAuthor,
                    "commentText"=>$commentText,
                    "publicDate"=>$dateComment]);
            $this->_queryExecute($query);

    }

//---------------------------------------------------------GET FUNCTIONS

    //-------------------------------------[Get Comments info]

    /**
     * Return pdo obj which contains comments by article or null
     * @param $idArticle
     * @return bool
     */
    public function getArticleComments($idArticle) {

        if(isset($idArticle)) {

            $query = new \core\Database_Query('comment');
            $query->select("userLogin, publicDate, commentText")
                ->join("user", "user.idUser=comment.idUser")
                ->where(["idArticle"=>$idArticle]);

            $commentRes = $this->_queryExecute($query);

            if ($commentRes) {
                return $commentRes;
            }

            return false;
        } else {
            return false;
        }
    }


    /**
     * Return article count or false
     * @param $idArticle
     * @return bool
     */
    public function getArticleCommentsCount($idArticle) {


        if(isset($idArticle)) {

            $query = new \Core\Database_Query('comment');
            $query->select("COUNT(*) AS articleComCount");

            $result = $this->_queryExecute($query);;

            if ($result) {
                $result = $result->fetch();
                if (!is_null($result['articleComCount'])) {
                    return ($result["articleComCount"]);
                } else {
                    return false;
                }
            }

            return false;
        } else {
            return false;
        }
    }


    //------------------------------------[Get Score info]

    /**
     * Return avg article score
     * @param $idArticle
     * @return bool
     */
    public function getAverageArticleScore($idArticle)
    {

        if(isset($idArticle)) {


            $query = new \Core\Database_Query('score');
            $query->select("AVG(score) AS Average")
                ->where(["idArticle"=>$idArticle]);

            $result = $this->_queryExecute($query);
            if ($result) {
                    $resultAverageScore = $result->fetch();
                    if (is_numeric($resultAverageScore['Average']))
                        return $resultAverageScore['Average'];
                    else {
                        return 0;
                    }

            }

            return false;
        } else {
             return false;
        }
    }

//-------------------------------------------------------CHECK FUNCTIONS

    //-----------------------------------[User Score Check]

    /**
     * Check user score by article
     * @param $idArticle
     * @param $idUser
     * @return bool
     */
    public function checkUserScoreArticle($idArticle, $idUser) {

        if(isset($idArticle) && isset($idUser)) {


            $query = new \Core\Database_Query('score');
            $query->select('COUNT(*) AS userScore')
                ->where(["idArticle"=>$idArticle, "idUser" => $idUser]);


            $resQuery = $this->_queryExecute($query);
            if ($resQuery) {
                $res = $resQuery->fetch();
                return ($res['userScore']);
            }
            return false;
        } else {
            return false;
        }
    }

}