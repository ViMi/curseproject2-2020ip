<?php

namespace  Core;

class Database_Query {

protected $type;
protected $fields;
protected $where;
protected $table;

protected $updateSet;
protected $join;

protected $insertSet;

function __construct($tableName) {
    $this->type = null;
    $this->fields = '*';
    $this->table = $tableName;
    $this->where = [];
    $this->join = [];
    $this->updateSet = [];
    $this->insertSet = [];
}


public function select($fields='*') {
    $this->type='SELECT';
    $this->fields=$fields;
    return $this;
}


public function delete() {
    $this->type = 'DELETE';
    return $this;
}


public function insert($fields) {
    $this->type = 'INSERT INTO';
    $this->insertSet=$fields;
    return $this;
}


public function update($row) {
    $this->type = "UPDATE";
    $this->updateSet = $row;
    return $this;
}


public function join($joinTable, $onCondition) {
    array_push($this->join, " JOIN {$joinTable} ON {$onCondition}");
    return $this;
}


public function where($condition) {
    if(isset($condition)) {
        if(is_string($condition)) {
            array_push($this->where, $condition);
        } elseif (is_array($condition)) {
            $this->where = array_merge($this->where, $condition);
        }
    }

    return $this;
}




public function getSQL()
{
    switch($this->type)
    {
        case 'SELECT':
        {

            $fieldStr = $this->_prepFieldList();

            $sql = "SELECT {$fieldStr} FROM {$this->table} ";

            if(!empty($this->join)) {
                $join = implode(' ', $this->join);
                $sql = $sql.$join;
            }

            if(!empty($this->where)){
                $sql = $sql." WHERE ".$this->_wherePart();
            }

            return ['sql' => $sql, 'params' => $this->_prepParam($this->where)];
        } break;


        case 'DELETE':
        {
            $sql = "DELETE FROM {$this->table}";

            if(!empty($this->where)){
                $sql = $sql." WHERE ".$this->_wherePart();
            }

            return ['sql' => $sql, 'params' => $this->_prepParam($this->where)];

        } break;


        case 'INSERT INTO':
        {

            $column = array_keys($this->insertSet);
            $values = array_values($this->insertSet);
            $columnList = implode(', ', $column);

            $valuesParamsList = [];
            $params = [];
            foreach ($this->insertSet as $key => $item) {
                array_push($valuesParamsList, ":".$key);
                $params[':'.$key] = $item;
            }

            $values = implode(', ', $valuesParamsList);

            $sql = "INSERT INTO {$this->table} ({$columnList}) VALUES ({$values});";
            return ['sql' => $sql, 'params' =>  $params];
        }

        case 'UPDATE':
        {

            $valuesParamsList = [];
            $valueSet = [];
            $params = [];
            foreach ($this->updateSet as $key => $item) {
                array_push($valuesParamsList, ":".$key);
                array_push($valueSet, $this->table.".".$key."=:".$key);
                $params[':'.$key] = $item;
            }

            $setList = implode(', ', $valueSet);

            $sql = "UPDATE {$this->table} SET {$setList}";

            if(!empty($this->where)) {
                    $sql = $sql . " WHERE " . $this->_wherePart();
            }

            $params = array_merge($this->_prepParam($this->where), $this->_prepParam($this->updateSet));

            return ['sql' => $sql, 'params' => $params];
        }
    }
return null;
}



//-----------------------------SERVICES FUNCTION

    protected function _wherePart() {

        if(!empty($this->where)) {

            $fieldList = array_keys($this->where);
            $valueKeys = array_values($this->where);
            $whereComponents = [];
            foreach($fieldList as $item) {
                array_push($whereComponents, "{$item} = :{$item}");
            }
            $wherePart = implode(' AND ', $whereComponents);
        }
        return $wherePart;
    }


    protected function _prepParam($row) {
        $params = [];
        foreach ($row as $key=>$item)
        {
            $params[':'.$key] = $item;
        }

        return $params;
    }


    protected function _prepFieldList()
    {
        if(is_string($this->fields)) {
            return  $field_str = $this->fields;
        } elseif (is_array($this->fields)) {
            return $field_str = implode(',', $this->fields);
        } else {
            return null;
        }
    }


}