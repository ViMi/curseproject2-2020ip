<?php

namespace Core;

class Core {

    public function __construct() {

        $this->route();

    }


    public function route() {

        global $currentPage;
        global $database;
        global $CMSConfig;

        $DBPass = $CMSConfig['DBConfig']['DBPass'];
        $DBUser = $CMSConfig['DBConfig']['DBUser'];
        $DBHost = $CMSConfig['DBConfig']['DBHost'];
        $DBName = $CMSConfig['DBConfig']['DBName'];
        $DBType = $CMSConfig['DBConfig']['DBType'];

        $database = new \Core\Database($DBPass, $DBUser, $DBHost, $DBName, $DBType);

        $controllerName = 'Main';
        $actionName = 'action_index';


        $routes = explode('/', $_SERVER['REQUEST_URI']);


        if(!empty($routes[1])){
            $trueName = explode('?',$routes[1]);
            $currentPage = $controllerName = $trueName[0];
        }

        if(!empty($routes[2])){
            $actionName = $routes[2];
        }

        $modelName ='Model_'.$controllerName;
        $controllerName = "Controller_".$controllerName;


        $modelFile = strtolower($modelName).'.php';
        $modelPath = "application/models/".$modelFile;


        if(file_exists($modelPath)){
            include "application/models/".$modelFile;
        }

        $controllerFile = strtolower($controllerName).'.php';
        $controllerPath = "application/controllers/".$controllerFile;


        if(file_exists($controllerPath)){
           include "application/controllers/".$controllerFile;
        } //else {
       //     Core::ErrorPage404();
        //}


        $modelName ='\Model\\'.$modelName;
        $controllerName = "\Controller\\".$controllerName;


        $controller = new $controllerName;
        $action = $actionName;


        $subType = explode('/', $_GET['type']);
        if($subType[1]){
            $_GET['type'] = $subType[0];
        }

        $type = $_GET['type'];


        if(method_exists($controller, $action)){
            $controller->$action();
        } else {
            Echo "Not Find Method in Controller<br><br><hr>";
            //Core::ErrorPage404();
        }

    }

    function ErrorPage404() {
        $host = 'http://'.$_SERVER['HTTP_HOST'].'/';
        header('HTTP/1.1 404 Not Found');
        header("Status: 404 Not Found");
        header('Location:'.$host.'404');
    }

}