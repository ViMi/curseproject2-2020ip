<?php
namespace Core;

class View{

    function generate($contentView, $templateView, $data = null) {


        if(is_array($data)) {
            extract($data, EXTR_OVERWRITE);
        }

        include "application/views/".$templateView;

    }
}