<?php

namespace  Core;

class Database {


    public $pdo = null;

    public function __construct($DBPass, $DBUser, $DBHost, $DBName, $DBType) {
        if(!$this->pdo) {
            try {

                $this->pdo = new \PDO("$DBType:host=$DBHost; dbname=$DBName; charset=utf8", $DBUser, $DBPass);
                $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

            } catch (\PDOException $e) {
                echo $e->getMessage();
            }

        }
    }


    public function executeQuery(Database_Query $query) {

        try {
            $res = $query->getSQL();

            $prepQuery = $this->pdo->prepare($res['sql']);


            $prepQuery->execute($res['params']);
            return $prepQuery;

        } catch(\PDOException $e) {
            echo $e->getMessage()."<hr>";
        }
        return null;
    }


}