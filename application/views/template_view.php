<?

global $currentPage;

$pageType = 'main';
if(isset($_GET['type'])){
    $pageType = $_GET['type'];
}


$getData = "type=$pageType";

if($_GET['id']){
    $getData = $getData."&id=".$_GET['id'];
}

if(isset($_GET['subType'])){
    $getData = $getData."&subType=".$_GET['subType'];
    $subType = $_GET['subType'];
}

$colorHeader = 'blue-bg';
$colorMain = 'yellow-bg';

switch($pageType) {
    case 'gallery':
        {
            $colorHeader = 'yellow-bg';
            $colorMain = 'red-bg';
        }
        break;

    case 'news':
        {
            $colorHeader = 'red-bg';
            $colorMain = 'blue-bg';
        }
        break;

    case 'videoArticle':
        {
            $colorHeader = 'grey-bg';
            $colorMain = 'black-bg';
        }
        break;


    case 'author':
    case 'admin':
    case 'article':
        {
            $colorHeader = 'white-bg';
            $colorMain = 'grey-bg';
        }
        break;

    case 'reg':
    case 'update':
        {
        $colorHeader = 'red-bg';
        } break;

}

?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <title> <? echo $pageType;?> </title>

    <link rel="stylesheet" href="css/styleGlobal.css"/>
    <script src="js/jquery-3.5.0.js"></script>

    <script src="js/jsHTML5Notification.js"></script>
    <script src="js/jsGlobal.js"></script>

    <?
        switch ($pageType) {

            case'404':
                {
                    echo " 
                        <link rel=\"stylesheet\" href=\"css/style404.css\">
                        ";
                }break;


            case 'video':
            case 'gallery':
            case 'news':
                {
                    echo"
                        <link rel=\"stylesheet\" href=\"css/styleArticle.css\"/>
                        <link rel=\"stylesheet\" href=\"css/styleHeader.css\"/>
                        <link rel=\"stylesheet\" href=\"css/styleGallery.css\"/>
                        <link rel=\"stylesheet\" href=\"css/styleSearch.css\"/>
    
                        <script src=\"js/jsGlobal.js\"></script>
                        
                        <script src=\"js/jsGalleryHiddenAudioPlay.js\"></script>
                        <script src=\"js/jsTypingAudioPlay.js\"></script>
                        <script src=\"js/jsScrollNotification.js\"></script>
                        <script src=\"js/ajaxLoad.js\"></script>
                       
                    ";
                }break;


            case 'videoArticle':
                {
                    echo"
                        <link rel=\"stylesheet\" href=\"css/styleVideoPlayer.css\"/>
                        ";
                }
            case 'message':
                echo "<link rel=\"stylesheet\" href=\"css/styleMessage.css\">";
            case 'author':
                echo "<link rel=\"stylesheet\" href=\"css/styleHonorRules.css\">";
            case 'user':
            case 'admin':
                echo "<link rel=\"stylesheet\" href=\"css/styleAdmin.css\">";
            case 'article':
                {
                echo"
                    
                      <link rel=\"stylesheet\" href=\"css/styleArticleItem.css\"/>
                      <link rel=\"stylesheet\" href=\"css/styleArticle.css\"/>
                      <link rel=\"stylesheet\" href=\"css/styleComments.css\"/>
                      <link rel=\"stylesheet\" href=\"css/styleHeader.css\"/>
                      <link rel=\"stylesheet\" href=\"css/styleFooter.css\"/>
                      <link rel=\"stylesheet\" href=\"css/styleUserPage.css\">
                      
                      <script src=\"js/jsTypingAudioPlay.js\"></script>
                      <script src=\"js/jsNotificationTextContent.js\"></script>
                ";
                }break;


            case 'update':
            case 'reg':
                {
                echo "  
                     <link rel=\"stylesheet\" href=\"css/styleHeader.css\"/>
                     <link rel=\"stylesheet\" href=\"css/styleRegForm.css\"/>
                   
                      <script src=\"js/jsTypingAudioPlay.js\"></script>
                    ";
                }break;


            case 'main':
                {
                echo "
                      <link rel=\"stylesheet\" href=\"css/styleMain.css\"/>
                     ";
                }break;
        }

    ?>


</head>


<body>

<?php
/***
 * variable of main part of page
 * @var $contentView
 */
include 'application/views/'.$contentView; ?>

</body>

</html>
