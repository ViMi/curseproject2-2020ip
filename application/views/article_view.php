<?php

include "subviews/independent_element/header_view.php";

?>

<main class="<?echo $colorMain;?>">
<section>

<?php

$id = $_GET['id'];

    include "subviews/independent_element/article_Information_view.php";


    switch($pageType)
    {
        case 'videoArticle':
            {
                include "subviews/items/video_player_item_view.php";

            }break;

        case 'article':
            {
                /***
                 * Main part of article
                 * @var $articleContent
                 */
             echo " 
                <div class='article-content-wrap'>
                    <span class='article-content-text'>
                        $articleContent
                        </span>
                </div>";

            }break;
    }


    include "subviews/independent_element/comment_form_view.php";

    include "subviews/items/comment_item_view.php";

    include "subviews/independent_element/footer_view.php";

?>

</section>

</main>