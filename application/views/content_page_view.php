<?
include 'subviews/independent_element/header_view.php';

function getStyle($i)
{
    if ($i % 2 === 0) {

        return 'black-bg text-color-white';

    } else if ($i % 3 === 0) {

        return 'red-bg';
    }
    return 'blue-bg';

}



$sectionClasses = "flex column articles";
$sectionRowClass = "flex row";
$elemInRow = 3;

if ($pageType == 'gallery') {
    $sectionClasses = $sectionClasses . ' gallery';
    $sectionRowClass = "flex gallery-row";
    $elemInRow = 4;

}



?>

<script>
/*
    let eventObj =
        {
            "onmouseenter":addAudioGallery,
            "onmouseleave":removeAudioGallery,
        };

    let jsonPath = "<?echo "$pageType".'.json';?>";
    let type = "<?echo "$pageType";?>";
    let styleRow = "<?echo "$sectionRowClass";?>";

    $(document).ready(

    function(e){
    AjaxLoad(eventObj, jsonPath, styleRow, type)
    });
*/
</script>



<main class=<?echo $colorMain?>>

    <section class="<?echo $sectionClasses; ?>" id="main-section">


        <?

     //   if($pageType !== 'gallery') {

            include 'subviews/independent_element/search_view.php';


            global $searchResultInfo;
            global $result;

            if (isset($_GET['pageContent'])) {
                $pageNum = $_GET['pageContent'];
            }

            $count = 0;
            $articleInfoClass = '';
            $articlePlaceholder = '';

            $countItem = 0;

            $count = ceil($result['count'] / $elemInRow);


            if ($searchResultInfo) {

                for ($i = 0; $i < $count; ++$i) {
                    echo "<div class=\"$sectionRowClass\">";

                    for ($j = 0; $j < $elemInRow; ++$j, ++$countItem) {

                        if ($countItem < $result['count']) {


                            $res = $searchResultInfo->fetch();


                            $articleName = $res['articleName'];
                            $articlePath = $res['articlePath'];
                            $posterPath = $res['articleHeaderPath'];

                            if ($pageType != 'gallery')
                                $articleInfoClass = getStyle($countItem);

                            $articleId = $res['idArticle'];

                            switch ($pageType) {

                                case 'video':
                                    {
                                        include "application/views/subviews/items/video_item_view.php";
                                    }
                                    break;

                                case 'news':
                                    {
                                        include "application/views/subviews/items/news_item_view.php";
                                    }
                                    break;

                                case 'gallery':
                                    {
                                        include "application/views/subviews/items/poster_item_view.php";
                                    }
                                    break;

                            }
                        }

                    }

                    echo '</div>';
                }
            } else {
                echo "<script>alert('False result search return');</script>";
            }

            if (!$result['count']) {
                echo "<script>alert('Not Find');</script>";
            }
   //     }
        ?>

    </section>

</main>