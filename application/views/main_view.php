<main>
    <section class="section-menu">

        <div class="yellow-bg">
            <div>
                <a href="content_page?type=video">
                    VIDEO
                </a>
            </div>
        </div>

        <div class="black-bg">
            <div>
                <a class="text-color-white" href="content_page?type=news">
                    NEWS
                </a>
            </div>
        </div>

        <div class="red-bg">
            <div>
                <a href="content_page?type=gallery">
                    GALLERY
                </a>
            </div>
        </div>

    </section>

    <section class="main-placeholder black-bg">
        <div class="yellow-bg flex">
            <div class="text">
                <div class="text-vert">
                    MUSIC
                </div>
                <div class="block blue-bg">
                    <div class ="black-bg">
                        <div class="white-bg">
                            <div class="red-bg">

                            </div>

                        </div>

                    </div>
                </div>
                <div class="text-hor">
                    NEWS
                </div>
            </div>

            <div class="pictures-blocks">

                <div class="picture third">
                    <img src="resources/pictures/music-poster2.jpg" alt="picture-poster">
                </div>

                <div class="picture first">
                    <img src="resources/pictures/music-poster3.jpg" alt="picture-poster">
                </div>

                <div class="picture second">
                    <img src="resources/pictures/music-poster1.jpg" alt="picture-poster">
                </div>

            </div>

            <div class="black-bg blocks-black"></div>

            <div class="red-bg blocks-red"></div>

            <div class="blue-bg blocks-blue"></div>

        </div>

        <div class="text-color-white text-frame">
            <pre>Mystic Moon News<br>     all rights reserved :-)
            </pre>
        </div>
    </section>

</main>
