<?php
/***
 * Controller variable set
 * @var $articleComments
 */
if(!is_null($articleComments)) {

    while ($res = $articleComments->fetch()) {

        $commentText = $res['commentText'];
        $commentPublicDate = $res['publicDate'];
        $commentAuthor = $res['userLogin'];

        echo "<div class=\"black-bg flex comment-item\">
    <div class=\"grey-bg comment-info\">
        <div>Author: <br/><span class=\"red-bg\">$commentAuthor</span></div>
        <div>Public date: <br/><span class=\"red-bg\">$commentPublicDate</span></div>
    </div>

    <div class=\"grey-bg comment-text\">
            <span> $commentText </span>
    </div>
</div>";

    }
}