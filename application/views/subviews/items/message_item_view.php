
<?php

/***
 *Controller variable set
 * @var $message
 * @var $sender | $getter
 */
if(!is_null($message)) {

if(isset($sender)) {
    $senderName = $sender;
    unset($sender);
} elseif (isset($getter)) {
    $getterName = $getter;
    unset($getter);
}


    while ($res = $message->fetch()) {

        $messageText = $res['message'];
        $date = $res['sendDate'];
        $them = $res['them'];

        if($getterName) {
            $senderName = $res['userLogin'];
        } elseif ($senderName) {
            $getterName = $res['userLogin'];
        }


        echo "
<div class=\"black-bg flex message-item\">
    <div class=\"grey-bg message-info\">
        <div>Getter: <br/><span class=\"yellow-bg\">$getterName</span></div>
        <div>Sender: <br/><span class=\"yellow-bg\">$senderName</span></div>
        <div>Send Date: <br/><span class=\"yellow-bg\">$date</span></div>
        <div>Them: <br/><span class=\"yellow-bg\">$them</span></div>
    </div>

    <div class=\"grey-bg message-text\">
            <span> $messageText </span>
    </div>
</div>";
    }
}