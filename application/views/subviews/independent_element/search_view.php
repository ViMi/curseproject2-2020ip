<div class="red-bg search">

    <form class="flex" action="<?echo "$currentPage?type=$pageType";?>/search" method="post">

        <div class="flex column">

            <input type="text" placeholder="search name" name="searchNameField" max="32">


            <div>
            <button type="submit">Search</button>

            <select name="typeOfSearch">
                <option value="Name" selected>Name</option>
                <option value="authorName">Author</option>
            </select>

            <select name="articleType">
                <option value="all" selected>All</option>
                <option value="Metal">Metal</option>
                <option value="Other">Other</option>
                <option value="Culture">Culture</option>
                <option value="PopMusic">Pop</option>
            </select>

            </div>
        </div>

    </form>
</div>

