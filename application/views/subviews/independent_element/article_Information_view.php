<?
/***
 * Controller varialbe set
 * @var $articleName
 * @var $articleInfo
 */
$articleName = $articleInfo['articleName'];
$author = $articleInfo['userLogin'];

$articlePath = $articleInfo['articlePath'];
$publicDate = $articleInfo['publicDate'];



if($articleInfo['articleHeaderPath'])
    $articleHeaderPath = "resources/pictures/articles/".$articleInfo['articleHeaderPath'];
else
    $articleHeaderPath = "resources/pictures/articles/1.jpg";

if(!$articleName)
    $articleName = "Unknown";
if(!$publicDate)
    $publicDate = "Unknown";
if(!$author)
    $author = "Unknown";



echo " <div class=\"image-article flex column\" style=\"background-image: url($articleHeaderPath);\">
        <div class=\"text-image flex column\">
            <div>
               $articleName
            </div>
        </div>
    </div>";


$articleInfoStyle = 'info-item yellow-bg';
$articleInfoBoxStyle = 'article-info black-bg';
$articleInfoSubStyle = 'red-bg';


$scoreForm = '';
if($_SESSION['id']){

   if(!$checkUserScore) {
       $scoreForm = "    <form action='$currentPage?$getData&/addArticleScore' method='post'>
            <label>You Score <input type=\"number\" name='userScore' max='10' min='0'></label>
                <button type=\"submit\">Submit score</button>
            </form>";
   }
}


echo "<div class='$articleInfoBoxStyle'>
        <div class='$articleInfoStyle'>Publishing date: <span class='$articleInfoSubStyle'> $publicDate</span></div>
        <div class='$articleInfoStyle'>average score: <span class='$articleInfoSubStyle'>$averageArticleScore</span></div>
        <div class='$articleInfoStyle'>Author: <span class='$articleInfoSubStyle'> $author </span></div>
        <div class='$articleInfoStyle'>
        $scoreForm
        </div>
    </div>";

