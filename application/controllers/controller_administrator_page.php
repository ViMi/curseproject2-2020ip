<?php

namespace  Controller;
include 'controller_create_article.php';

class Controller_Administrator_page extends Controller_Create_article {

//---------------------------------------------------MAIN FUNCTION
    function __construct()
    {
        $this->model = new \Model\Model_Administrator_page();
        $this->view = new \Core\View();
    }

    function action_index($data = null)
    {

        if(is_null($data) && $_SESSION['id']){
            $this->typeHub();
        }

        $dataView = Array(
            "adminInfo" => $data
        );

        $this->view->generate("administrator_page_view.php", "template_view.php", $dataView);
    }


//---------------------------------------------------------------OTHER

    function changeUserRole() {

        if(isset($_POST['name']) && isset($_POST['newRole'])){


            $this->model->changeUserRole($_POST['name'], $_POST['newRole']);

        }

        $this->action_index();
    }

//------------------------------------------------HUBS

    function typeHub()
    {
        switch($_GET['subType'])
        {
            case'article':
                {
                    $this->getAllArticleInfo();
                }break;

            case'scores':
                {
                    $this->getAllScores();
                }break;

            case'comments':
                {
                    $this->getAllComments();
                }break;

            case'user':
                {
                    $this->getAllRegUser();
                }break;

            case'changeRole':
                {
                    $this->getAllRoles();
                }break;
        }
    }



function adminHub() {

    switch($_GET['subType'])
    {
        case'article':
        {
            $this->articleHub();

        }break;

        case 'scores':
        {
            $this->scoresHub();
        }break;

        case 'comments':
        {
            $this->commentHub();
        }break;

        case 'user':
        {

            $this->userHub();
        }break;


        case 'changeRole':
        {
            $this->getAllRoles();
        }break;

    }
}


function articleHub() {
    if($_POST['adminType'] == 'getAll'){
        $this->action_index();
    }

    else {

            switch ($_POST['adminType']) {

                case 'removeByName':
                {
                    $this->removeArticleByName();
                }break;

                case'removeById':
                    {
                        $this->removeArticleById();
                    }break;


                case'removeByAuthor':
                    {
                        $this->removeArticleAuthor();
                    }break;

                default:
                    {

                        $this->action_index();
                    }break;
            }

        }
}


function commentHub() {

          switch ($_POST['adminType']) {

                case'removeById':
                    {
                        $this->removeCommentById();
                    }
                    break;

                case'removeByAuthor':
                    {
                        $this->removeCommentByAuthor();
                    }
                    break;

                case 'removeByArticle':
                    {
                        $this->removeCommentByArticle();
                    }
                    break;

                case 'getAllByArticle':
                    {
                        $this->getAllCommentsByArticle();
                    }
                    break;


                case 'getAllByAuthor':
                    {
                        $this->getAllCommentsByAuthor();
                    }
                    break;

                default:
                    {

                        $this->getAllComments();
                    }
                    break;
            }

}


function scoresHub() {
    if($_POST['adminType'] == 'getAll')
        $this->getAllScores();
    else{
        $this->getAllScores();
    }
}


function userHub() {

    if($_POST['adminType'] == 'getAll'){
        $this->getAllRegUser();
    }

    else {

            switch ($_POST['adminType']) {

                case'removeById':
                    {
                        $this->removeUserById();
                    }break;


                case 'removeByName':
                {
                    $this->removeUserByName();
                }

                case 'getAllAdmins':
                    {
                        $this->getAllAdmins();
                    }

                case 'getAllAuthors':
                {
                    $this->getAllAuthors();
                }break;

                case 'getAllUser':
                {
                    $this->getAllUsers();
                }break;

                default:
                    {
                        $this->getAllRegUser();
                    }break;
            }


    }

}

//------------------------------------------------INFORMATION FUNCTION

    //---------------------[Role Info]
    function getAllRoles() {
     $data = $this->model->getAllRoles();
     $this->action_index($data);
    }

    //---------------------[Article Info]

    function getAllArticleInfo() {
        $data = $this->model->getAllArticleInfo();
        $this->action_index($data);
    }


    function getAllScores() {
        $data = $this->model->getAllScores();
        $this->action_index($data);
    }

    //-----------------------[Users Info]

    function getAllUsers() {
        $data = $this->model->getAllUsers();
        $this->action_index($data);
    }


    function getAllAuthors() {
        $data = $this->model->getAllAuthors();
        $this->action_index($data);
    }


    function getAllAdmins() {
        $data = $this->model->getAllAdmins();
        $this->action_index($data);
    }


    function getAllRegUser() {
        $data = $this->model->getAllRegUser();
        $this->action_index($data);
    }


    //----------------------[Comments]

    function getAllComments() {
        $data = $this->model->getAllComments();
        $this->action_index($data);
    }


    function getAllCommentsByArticle() {
        $data = null;
        if(isset($_POST['name'])) {
          $data = $this->model->getAllCommentsByArticle($_POST['name']);
        }

        $this->action_index($data);
    }


    function getAllCommentsByAuthor() {
        $data = null;
        if(isset($_POST['name'])) {
            $data = $this->model->getAllCommentsByAuthor($_POST['name']);
        }

        $this->action_index($data);
    }


//---------------------------------------------------------REMOVE FUNCTION

    //-----------------------[User Remove]

    function removeUserById() {


        if(isset($_POST['name'])) {
            $this->model->removeUserById($_POST['name']);
        }

        $this->action_index();
    }


    function removeUserByName() {

        if(isset($_POST['name'])){
            $this->model->removeUserByName($_POST['name']);
        }

        $this->action_index();

    }


    //------------------------[Article Remove]

    function removeArticleById() {

        if(isset($_POST['name'])){
            $this->model->removeArticleById($_POST['name']);
        }

        $this->action_index();

    }


    function removeArticleAuthor() {

        if(isset($_POST['name'])) {
            $this->model->removeArticleAuthor($_POST['name']);
        }

        $this->action_index();
    }


    //------------------------[Comment Remove]

    function removeCommentByAuthor() {

        if(isset($_POST['name'])) {
            $this->model->removeCommentByAuthor($_POST['name']);
        }
        $this->action_index();

    }


    function removeCommentById() {

        if(isset($_POST['name'])) {
          $this->model->removeCommentById($_POST['name']);
        }
        $this->action_index();

    }


    function removeCommentByArticle() {

        if(isset($_POST['name'])) {
            $this->model->removeCommentByArticle($_POST['name']);
        }

        $this->getAllComments();

    }


}


