<?php
namespace  Controller;

include "controller_user_page.php";

class Controller_Message_Page extends \Controller\Controller_User_Page {

    function __construct()
    {

        $this->model = new \Model\Model_Message_page();
        $this->view = new \Core\View();


    }


    function action_index($data = null) {

        $this->view->generate("message_view.php", "template_view.php", $data);

    }

//--------------------------------------OTHER

    function addNewMessage() {
        if(isset($_POST['message']) && isset($_POST['name']) && isset($_POST['them'])) {
            $this->model->addNewMessage($_SESSION['login'], $_POST['name'], $_POST['message'], $_POST['them']);
        }
        $this->action_index();
    }

//--------------------------------------HUBS


    function sendHub() {

        switch($_POST['messageType']) {

            case 'getAll':
                {
                    $this->getAllSendMessage();
                }break;

            case 'getByDate':
                {
                    $this->getAllSendMessageByDate();
                }break;

            case 'getByGetter':
                {
                    $this->getAllSendMessageByGetterName();
                }break;

            case 'getBySender':
            {
            //    $this->getAllSendMessageBySenderName();
            }break;

            default :
            {
                $this->getAllSendMessage();
            }
        }

    }


    function getHub() {

        switch($_POST['messageType']) {
            case 'getAll':
                {
                    $this->getAllMessage();
                }break;

            case 'getByDate':
                {
                    $this->getAllMessageByDate();
                }break;

            case 'getBySender':
                {
                    $this->getAllMessageBySenderName();
                }break;

            default :
            {
                $this->getAllMessage();
            }
        }

    }


    function messageHub() {


        switch($_GET['subType'])
        {
            case 'get':
                $this->getHub();
                return 0;
            case 'send':
                $this->sendHub();
                return 0;

        }

        return -1;
    }

//-----------------------------------------------GET

    function getAllMessage() {
        $data = $this->model->getAllMessage($_SESSION['id']);
        $this->action_index($data);
    }


    function getAllMessageByDate() {
        if(isset($_POST['date'])) {
            $data =  $this->model->getAllMessageByDate($_POST['date'], $_SESSION['id']);
        }
        $this->action_index($data);
    }


    function getAllMessageBySenderName() {
        if(isset($_POST['name'])) {
            $data =  $this->model->getAllMessageBySenderName($_POST['name'], $_SESSION['id']);
        }
        $this->action_index($data);
    }

//-----------------------------------------GET SEND

    function getAllSendMessage() {
        $data = $this->model->getAllSendMessage($_SESSION['id']);
        $this->action_index($data);
    }


    function getAllSendMessageByGetterName() {
        if(isset($_POST['name'])) {
            $data =  $this->model->getAllSendMessageByGetterName($_POST['name'], $_SESSION['id']);
        }
        $this->action_index($data);
    }


    function getAllSendMessageByDate() {
        if(isset($_POST['date'])) {
            $data =  $this->model->getAllSendMessageByDate($_POST['date'], $_SESSION['id']);
        }
        $this->action_index($data);
    }


}