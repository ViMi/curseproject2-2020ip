<?php

namespace Controller;


class Controller_Content_Page extends \Core\Controller {


    function __construct()
    {
        $this->model = new \Model\Model_Content_Page();
        $this->view = new \Core\View();
    }


    function action_index()
    {

        global $searchResultInfo;
        global $result;


        $data = $this->getData();
        $this->model->search($data, $result, $searchResultInfo);

        $data = array(
            "searchResultInfo" => $searchResultInfo,
            "result" => $result
        );


        $this->view->generate("content_page_view.php", "template_view.php", $data);

    }


    protected function getData() {

        if($_GET['type'] == 'gallery'){
            $direction = 3;
        } elseif ($_GET['type'] == 'news'){
            $direction = 1;
        }else {
            $direction = 2;
        }


        $searchNameField = $_POST['searchNameField'];
        $typeOfSearch = $_POST['typeOfSearch'];
        $articleType = $_POST['articleType'];


        return $data = array(
            'direction' => $direction,
            'searchNameField' => $searchNameField,
            'typeOfSearch' => $typeOfSearch,
            'articleType' => $articleType
        );
    }


    function search() {
        $data = $this->getData();
        $this->model->search($data, $result, $searchResultInfo);
        $this->action_index();
    }


    function logInUser() {
        if (isset($_POST['login']) && isset($_POST['pass'])) {

            $login = htmlentities(strip_tags($_POST['login']));
            $pass = htmlentities(strip_tags($_POST['pass']));

            $userInfo = $this->model->logInUser($login, $pass);

            if($userInfo) {
                $_SESSION['login'] = $login;
                $_SESSION['pass'] = $pass;
                $_SESSION['id'] = $userInfo['idUser'];
                $_SESSION['logIn'] = true;
                $_SESSION['role'] = $userInfo['typeName'];
            } else {
                echo "<script>alert('user not found'); </script>";
            }

        }

        $this->action_index();
    }


    function exitUser() {
        if($_SESSION['logIn']){
            unset($_SESSION['login']);
            unset($_SESSION['id']);
            unset($_SESSION['logIn']);
            unset($_SESSION['role']);
            unset($_SESSION['pass']);
        }

        $this->action_index();
    }


}


