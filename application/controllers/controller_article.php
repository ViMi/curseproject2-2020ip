<?php

namespace Controller;

include 'controller_content_page.php';

class Controller_Article extends Controller_Content_Page {

    function __construct() {
        $this->model = new \Model\Model_Article();
        $this->view = new \Core\View();
    }


    function addComment() {

        if(isset($_POST['commentText']) && (strlen(trim(strip_tags($_POST['commentText']))) > 0)) {

            $loginAuthor = $_SESSION['id'];
            $dateComment = date('y.m.d');
            $idArticle = $_GET['id'];
            $commentText = $_POST['commentText'];

            $this->model->addArticleComment($idArticle, $loginAuthor, $commentText, $dateComment);

        }

        $this->action_index();

    }


    function addArticleScore() {

            $check = $this->checkUserScoreArticle();

            if (isset($_POST['userScore']) && !$check) {
                $idUser = $_SESSION['id'];
                $idArticle = $_GET['id'];
                $score = $_POST['userScore'];
                $this->model->addArticleScore($idArticle, $idUser, $score);
            }

        $this->action_index();

    }


    function checkUserScoreArticle(){

        if(isset($_SESSION['id'])) {
            $idUser = $_SESSION['id'];
            $idArticle = $_GET['id'];

            return $check = $this->model->checkUserScoreArticle($idArticle, $idUser);
        }

        return false;
    }


    function action_index() {



        $articleInfo = $this->model->getArticleInfo($_GET['id']);
        $averageArticleScore =  $this->model->getAverageArticleScore($_GET['id']);

        $filePath = 'resources/articleText/'.$articleInfo['articlePath'];
        $extension = explode('.', $articleInfo['articlePath']);


        if($extension[1] == 'txt')
            $articleContent = $this->model->readArticleTextFile($filePath);

        $articleComments = $this->model->getArticleComments($_GET['id']);

        $articleCommentsCount = $this->model->getArticleCommentsCount($_GET['id']);

        $checkUserScore = $this->checkUserScoreArticle();


        $data = Array(
            "articleInfo" => $articleInfo,
            "averageArticleScore" => $averageArticleScore,
            "articleContent" => $articleContent,
            "articleComments" => $articleComments,
            "articleCommentsCount" => $articleCommentsCount,
            "checkUserScore" => $checkUserScore
        );

        $this->view->generate("article_view.php", "template_view.php", $data);
    }

}