<?php

namespace Controller;

include 'controller_content_page.php';

class Controller_Registration extends Controller_Content_Page {

    function __construct() {
        $this->model = new \Model\Model_Registration();
        $this->view = new \Core\View();

    }


    function action_index() {
        $this->view->generate("registration_view.php", "template_view.php");
    }


    public function addNewUser() {


        if(isset($_POST['login']) && isset($_POST['pass']) && isset($_POST['passr'])) {
            if ($_POST['pass'] !== $_POST['passr']) {
                echo "<script> alert('Error of password');</script>";
            } else {
                $login = htmlentities(strip_tags($_POST['login']));
                $pass = htmlentities(strip_tags($_POST['pass']));
                $date = date('y.m.d');
                $userRole = 2;

                $this->model->addNewUser($login, $pass, $date, $userRole);
                $this->model->logInUser($login, $pass);
            }
        }

        $this->action_index();

    }


    public function removeUser() {

        $login = $_SESSION['login'];
        $pass = $_SESSION['pass'];

        $this->model->removeUser($login, $pass);

        $this->exitUser();

        $this->action_index();

    }


    public function updateUserInfo() {

        if(isset($_POST['login']) && isset($_POST['pass']) && isset($_POST['passr'])){
            if($_POST['pass'] !== $_POST['passr']){
                echo "<script> alert('Error of password');</script>";
            } else {

                $login = htmlentities(strip_tags($_POST['login']));
                $pass = htmlentities(strip_tags($_POST['pass']));

                $this->model->updateUserInfo($login, $pass, $_SESSION['id']);

                $this->logInUser();
            }
        }

        $this->action_index();

    }

}