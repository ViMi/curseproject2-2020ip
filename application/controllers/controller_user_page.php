<?php

namespace Controller;

include "controller_content_page.php";


class Controller_User_Page extends \Controller\Controller_Content_Page {

    function __construct()
    {

        $this->model = new \Model\Model_User_page();
        $this->view = new \Core\View();


    }


    function action_index($data = null) {

        $this->view->generate("user_page_view.php", "template_view.php", $data);

    }

}