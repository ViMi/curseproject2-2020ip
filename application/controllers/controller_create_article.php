<?php
namespace  Controller;

include 'controller_article.php';

class Controller_Create_Article extends Controller_Article {

//-----------------------------------------------------MAIN FUNCTIONS

    function __construct() {
        $this->model = new \Model\Model_Create_Article();
        $this->view = new \Core\View();

    }


    function action_index($data = null) {

        if(is_null($data)){
            $data = $this->getAllArticleByAuthor();
        }

        $dataView = Array(
            "authorInfo" => $data
        );

        $this->view->generate("create_article_view.php", "template_view.php", $dataView);

    }

//--------------------------------------------HUBS

    function authorHub() {


        switch($_GET['subType'])
        {
            case'article':
                {
                    $this->articleHub();

                }break;

            case 'createNew':
                {

                    $this->addNewArticle();
                }break;


            case 'jsonInit':
            {
                $this->model->jsonInit();
            }break;

            default:
            {
                $this->action_index();
            }

        }

    }


    function articleHub() {

        if($_POST['authorType'] == 'getAll')
        {
            $this->action_index();
        //    $this->model->_startJsonInit();
        }
        else
        {
            switch($_POST['authorType'])
            {
                case 'removeById':
                {
                    $this->removeArticleById();
                }

                case 'removeByName':
                {
                    $this->removeArticleByName();
                }

                default :
                {
                    $this->action_index();
                }
            }
        }

    }

//-------------------------------------------ADD NEW ARTICLE

     function addNewArticle() {

         $this->model->addNewArticle($_POST['textarea'], $_POST['name'], $_SESSION['id'], $_POST['type']);
         $this->action_index();
    }

//----------------------------------------------REMOVE FUNCTION

     function removeArticleById() {
         $this->model->removeArticleById($_POST['name'], $_SESSION['id']);
         $this->action_index();
     }


     function removeArticleByName() {


         $this->model->removeArticleByName($_POST['name'], $_SESSION['id']);
         $this->action_index();
     }

//-----------------------------------------------INFORMATION FUNCTION

    function getAllArticleByAuthor() {
        if(!isset($_POST['authorType']))
            return $this->model->getArticleInfoByAuthor($_SESSION['login']);
        $this->action_index($this->model->getArticleInfoByAuthor($_SESSION['login']));
    }
}