<?php
require_once  "application/config/configuration.php";

require_once "core/database_query.php";
require_once "core/database.php";
require_once "core/model.php";
require_once "core/controller.php";
require_once "core/Core.php";
require_once "core/view.php";


$core = new \Core\Core();
