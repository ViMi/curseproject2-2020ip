let ways =[
    ["resources/citeAudio/type/type1.mp3"],
    ["resources/citeAudio/type/type2.mp3"],
    ["resources/citeAudio/type/type3.mp3"],
    ["resources/citeAudio/type/type4.mp3"],
    ["resources/citeAudio/type/type5.mp3"]
];

document.addEventListener("DOMContentLoaded", initAudioElem);


function initAudioElem(){

    let inputs =  document.getElementsByTagName('input');
    let textAreas = document.getElementsByTagName('textarea');

    console.log(textAreas);


    for(let i = 0; i < inputs.length; ++i){
            inputs[i].addEventListener("input", addAudioType);
    }

    for(let i = 0; i < textAreas.length; ++i){
        textAreas[i].addEventListener('input', addAudioType);
    }

    return 0;
}


function addAudioType(){

    let way = ways[rand(0,4)];

    let audioElem = document.createElement('audio');
    audioElem.className= "hidden-audio";
    audioElem.setAttribute('name', 'hiddenAudioType');
    audioElem.autoplay = true;
    audioElem.innerHTML = `<source src=${way}>`;

    setTimeout(removeAudioType, 1000);

    document.body.appendChild(audioElem);

}


function removeAudioType() {

    let audioElem = document.getElementsByName('hiddenAudioType')[0];
    document.body.removeChild(audioElem);

}