(function($) {

        $.fn.textContentNotification =
            function(maxContentLength, spanElementId, textElemId) {
                let spanElem = document.getElementById(spanElementId);
                let textElem = document.getElementById(textElemId);

                console.log(spanElementId);
                console.log(textElemId);
                textElem.addEventListener('input',
                    function (){
                      if(spanElem)
                        spanElem.innerHTML = "Tokens "+textElem.value.length+"\\"+maxContentLength;

                        if(textElem.value.length >=maxContentLength){
                            textElem.value = textElem.value.slice(0,maxContentLength);
                            if(spanElem)
                            spanElem.style.color = 'red';
                        } else{
                            if(spanElem)
                            spanElem.style.color = 'black';
                        }
                    }
                    )
            }
    }
)(jQuery);
