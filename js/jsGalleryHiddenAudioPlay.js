document.addEventListener("DOMContentLoaded", initAudioElem);


function initAudioElem() {

    let workElements = document.getElementsByClassName("gallery-row");

    for(let i = 0; i < workElements.length; ++i){

        workElements[i].onmouseenter=addAudioGallery;
        workElements[i].onmouseleave=removeAudioGallery;

    }

    return 0;
}


function addAudioGallery() {

   let audioElem = document.createElement('audio');
    audioElem.className = "hidden-audio";
    audioElem.setAttribute('name', 'hiddenAudioGallery');
    audioElem.autoplay = true;

    audioElem.innerHTML = "<source src='resources/citeAudio/changePage.mp3'>";

    document.body.appendChild(audioElem);

}


function removeAudioGallery() {

    let audioElem = document.getElementsByName('hiddenAudioGallery')[0];
    document.body.removeChild(audioElem);

}