document.addEventListener("DOMContentLoaded", checkAccess);


let mailNotification = null;
let notifFlag = true;


function checkAccess() {

    let permission,

        timer = setTimeout( function() { permission = "default" }, 500 );
    Notification.requestPermission( function(state){ clearTimeout(timer); permission = state } );

    setInterval(isDocVisible,10000);
}


function isDocVisible() {

    if(document.hidden){

        setTimeout( function () {
                mailNotification = new Notification(
                    "One way Ticket", {
                    tag : "Time 1 min",
                    body : "One minute passed from you go away out, i miss you )",
                    icon : "../pictures/404.jpg",
                });
            },
            60000
        );
        notifFlag = false;
    }
    return 0;
}