let ELEMENTS_IN_ROW = 3;

function AjaxLoad(callbackObj, jsonPath, styleRow, type){

    $.getJSON('json/'+jsonPath, function(data) {

            let row = document.createElement('div');
            row.className=styleRow;

            let elemWrap = document.getElementById('main-section');

            for(let i = 0, c=0; i < data.length ;++i, ++c) {

                let template = _getTemplate(type, data[i], i);

                row.appendChild(template);

                _setEvent(callbackObj, row);

                if (c === ELEMENTS_IN_ROW) {

                    elemWrap.appendChild(row);
                    c = -1;
                    row = document.createElement('div');
                    row.className='flex gallery-row';
                } else if (c !== ELEMENTS_IN_ROW && i + 1 === data.length) {
                    elemWrap.appendChild(row);
                }

            }

        }
    );

}


function _getTemplate(type, data){
    switch(type) {

        case "gallery":
        {
            return _getGalleryTemplate(data['articlePath']);
        }break;

    }
}


function _setEvent(eventObj, obj){

    for(let key in eventObj) {

        obj[key]=eventObj[key];

    }
}


function _getGalleryTemplate(poster){

    let div = document.createElement('div');
    div.className='picture';

    let img = document.createElement('img');
    img.src=`resources/pictures/articles/${poster}`;
    img.alt=`${poster}`;

    div.appendChild(img);

    return div;

}


