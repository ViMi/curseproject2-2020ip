document.addEventListener("DOMContentLoaded", initComponent);

let section = null;

let HEIGHT_OF_SIZE_AREA = 390;

function initComponent() {
    section = document.getElementById('main-section');
    section.addEventListener('scroll', populate);
}


function populate() {


        if ( section.scrollTop >= HEIGHT_OF_SIZE_AREA) {

            let audioElem = document.createElement('audio');
            audioElem.className = "hidden-audio";
            audioElem.setAttribute('name', 'hiddenAudioEOF');
            audioElem.autoplay = true;
            audioElem.innerHTML = "<source src='resources/citeAudio/eof.mp3'>";

            document.body.appendChild(audioElem);

            setTimeout(function (){
                let audioElem = document.getElementsByName('hiddenAudioEOF')[0];
                document.body.removeChild(audioElem);
            }, 2000);

        }

}

